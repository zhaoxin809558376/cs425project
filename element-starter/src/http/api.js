import { Header } from 'element-ui';
import http from './http';

const HOST = 'http://59.110.63.211:3000';

const LOGIN = `${HOST}/customer/login`;

const REGISTER = `${HOST}/customer/register`;
// 获取客户信息
const CUSTOMERINFO = `${HOST}/customer/customer-info`;
// 更新客户信息
const UPDATECUSTOMERINFO = `${HOST}/customer/edit-customer-info`;
// 更新密码
const UPDATECUSTOMPERPASSWORD = `${HOST}/customer/update-customer-password`;
// 获取商品分类
const GETCATEGORY = `${HOST}/customer/goods/category`;
// 获取分类商品列表
const GETCATEGORYLIST = `${HOST}/customer/goods/list`;
// 获取产品详情
const GETPRODUCTDETAIL = `${HOST}/customer/goods/detail`;
// 添加商品到购物车
const ADDPRODUCTCART = `${HOST}/customer/cart/add`;
// 从购物车删除商品
const DELETEPRODUCTCART = `${HOST}/customer/cart/del`;
// 获取购物车列表
const GETCARTLIST = `${HOST}/customer/cart/list`;
// 获取地址列表
const GETADDRESSLIST = `${HOST}/customer/get-address-list`;
// 更新地址
const UPDATEADDRESSLIST = `${HOST}/customer/update-address`;
// 新增地址
const ADDADDRESSLIST = `${HOST}/customer/create-address`;
// 提交
const SUBMITCART = `${HOST}/customer/cart/submit`;
// 删除地址
const DELETEADDRESS = `${HOST}/customer/delete-address`;
// staff login
const STAFFLOGIN = `${HOST}/staff/login`;
// staff register
const STAFFREGISTER = `${HOST}/staff/register`;
// get warehouse list
const GETWAREHOUSELIST = `${HOST}/staff/warehouse-list`;
// add warehouse
const ADDWAREHOUSE = `${HOST}/staff/warehouse-add`;
// get staff info
const GETSTAFFINFO = `${HOST}/staff/staff-info`;
// update staff info
const UPDATESTAFFINFO = `${HOST}/staff/edit-staff-info`;

// delete warehouse
const DELETEWAREHOUSE = `${HOST}/staff/warehouse/delete`;
// update warehouse
const UPDATEWAREHOUSE = `${HOST}/staff/warehouse-update`;
// get product
const GETPRODUCT = `${HOST}/staff/stock`;
// add product
const ADDPRODUCT = `${HOST}/staff/goods/add`;
// delete product
const DELETEPRODUCT = `${HOST}/staff/goods/delete`;
// update product
const UPDATEPRODUCT = `${HOST}/staff/goods/edit`;

// search product
const SEARCHPRODUCT = `${HOST}/customer/goods/search`;
// add credit card
const ADDCREDIT = `${HOST}/customer/credit-card/add`;

const api = {
    login(params) {
        return http.post(LOGIN, params);
    },
    register(params) {
        return http.post(REGISTER,params);
    },
    customerInfo(params,headers) {
        return http.get(CUSTOMERINFO,params,headers);
    },
    updateCustomerInfo(params,headers){
        return http.post(UPDATECUSTOMERINFO,params,headers);
    },
    updateCustomerPassword(params,headers){
        return http.post(UPDATECUSTOMPERPASSWORD,params,headers);
    },

    getCategory(params){
        return http.get(GETCATEGORY,params);
    },

    getCategoryList(params){
        return http.get(GETCATEGORYLIST,params);
    },
    
    getProductDetail(params){
        return http.get(GETPRODUCTDETAIL,params)
    },

    addProductCart(params,headers){
        return http.post(ADDPRODUCTCART,params,headers);
    },

    deleteProductCart(params,headers){
        return http.post(DELETEPRODUCTCART,params,headers);
    },
    // 获取购物车列表
    getCartList(params,header){
        return http.get(GETCARTLIST,params,header);
    },
    // 获取地址列表
    getAddressList(params,headers){
        return http.get(GETADDRESSLIST,params,headers);
    },
    // 更新地址
    updateAddress(params,headers){
        return http.post(UPDATEADDRESSLIST,params,headers);
    },
    // 新增地址
    addAddressList(params,headers){
        return http.post(ADDADDRESSLIST,params,headers);
    },
    // 提交购物车
    submitCart(params,headers){
        return http.post(SUBMITCART,params,headers);
    },
    // 删除地址
    deleteAddress(params,headers){
        return http.post(DELETEADDRESS,params,headers)
    },
    // staff 注册
    staffRegister(params){
        return http.post(STAFFREGISTER,params)
    },
    // staff 登录
    staffLogin(params){
        return http.post(STAFFLOGIN,params)
    },
    // warehouse list
    getWarehouseList(params,headers){
        return http.get(GETWAREHOUSELIST,params,headers)
    },
    // add warehouse list
    addWarehouse(params,headers){
        return http.post(ADDWAREHOUSE,params,headers)
    },
    // get staff info
    getStaffInfo(params,headers){
        return http.get(GETSTAFFINFO,params,headers)
    },
    // update staff info
    updateStaffInfo(params,headers){
        return http.post(UPDATESTAFFINFO,params,headers)
    },
    // delete warehouse
    deleteWherehouse(params,headers){
        return http.post(DELETEWAREHOUSE,params,headers)
    },
    // update warehouse
    updateWarehouse(params,headers){
        return http.post(UPDATEWAREHOUSE,params,headers)
    },

    // staff get product
    getProduct(params,headers){
        return http.get(GETPRODUCT,params,headers)
    },

    // add product
    addProduct(params,headers){
        return http.post(ADDPRODUCT,params,headers)
    },
    // delete product
    deleteProduct(params,headers){
        return http.post(DELETEPRODUCT,params,headers)
    },
    // update product
    updateProduct(params,headers){
        return http.post(UPDATEPRODUCT,params,headers)
    },
    // search product
    searchProduct(params){
        return http.get(SEARCHPRODUCT,params)
    },

    // add credit card
    addCredit(params,headers){
        return http.post(ADDCREDIT,params,headers)
    },
}

export default api;