// 路由表

import VueRouter from 'vue-router';

// 引入组件
import main from '../page/main';
import Warehouse from '../page/warehouse';
import Product from '../page/product';
import Staff from '../page/staff';
import Management from '../page/management';
import Customer from '../page/customer';
import login from '../page/login';
import User from '../page/user';
import Shopping from '../page/shopping';
import Cart from '../page/cart';
import categoryP1 from '../page/categoryProduct1';
import categoryP2 from '../page/categoryProduct2';
import categoryP3 from '../page/categoryProduct3';
import categoryP4 from '../page/categoryProduct4';

const routes = [
    {
        path: '/',
        redirect: '/login'
    },
    {
        path: '/login',
        component: login,
    },
    {
        path: '/Management',
        component: Management,
        children:[
            {
                path:'/warehouse',
                component: Warehouse
            },
            {
                path:'/product',
                component: Product
            },
            {
                path:'/staff',
                component: Staff
            }

        ]
    },
    {
        path: '/Customer',
        component: Customer,
        children:[
            {
                path:'/shopping',
                component:Shopping,
                children:[
                    {
                        path:'/categoryProduct1',
                        component: categoryP1
                    },
                    {
                        path:'/categoryProduct2',
                        component: categoryP2
                    },
                    {
                        path:'/categoryProduct3',
                        component: categoryP3
                    },
                    {
                        path:'/categoryProduct4',
                        component: categoryP4
                    },
                ]
            },
            {
                path:'/user',
                component:User

            },
            {
                path: '/cart',
                component:Cart

            }
        ]
    }

];

const router = new VueRouter({
    routes // (缩写) 相当于 routes: routes
  })

const VueRouterPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(to){
    return VueRouterPush.call(this,to).catch(err=>err)
}


export default router;